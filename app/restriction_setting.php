<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class restriction_setting extends Model  {

    protected $connection = 'mysql';
    protected $table = 'restriction_setting';

    public $timestamps = false;
}
