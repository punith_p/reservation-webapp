<?php

namespace App\Http\Controllers;

use App\reservations;
use App\restriction_setting;
use Illuminate\Http\Request;

class ReservationController extends Controller
{
    public function modify(Request $request)
    {
        $validateData = $request->validate([
            'n' => 'required',
            'd' => 'required',
            'g' => 'required',
            'tz' => 'required',
        ]);
        if ($validateData){
            session(['n' => $request->n,'d' => $request->d,'g' => $request->g,'tz' => $request->tz]);
            return view('website.validateReservation');
        }else{
            return redirect()->back()->with('error','error in validation');
        }

    }

    public function validateReservation(Request $request)
    {
        $setting = session()->get('g');
        $users= $request->user_ids;
        $timestamp=strtotime($request->reservation_datetime);
        if ($setting == 'individual'){
            foreach ($users as $user){
                $isReservation=reservations::where('reservation_timestamp_utc',$timestamp)->where('user_id',$user)->get();
                if ($isReservation != null){
                    $response['restricted_user_ids'][]=$user;
                }
            }
        }else{
            $isReservation=reservations::where('reservation_timestamp_utc',$timestamp)->whereIn('id', $users)->get();
            if ($isReservation != null){
                $response['restricted_user_ids']=$users;
            }
        }
        if (isset($response['restricted_user_ids'])){
            $response['is_booking_restricted']=true;

        }else{
            $response['is_booking_restricted']=false;
            $response['restricted_user_ids']=[];
        }

        return response()->json($response);

    }
}
