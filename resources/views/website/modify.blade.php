@extends('layouts.layout')

@section('content')
    <section class="page-title o-hidden text-center grey-bg bg-contain animatedBackground"
             data-bg-img="/themes/base/frontassets/images/pattern/05.png">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="title">modify the Reservation Setting</h1>
                </div>
            </div>
        </div>
        <div class="page-title-pattern"><img class="img-fluid" src="themes/base/frontassets/images/bg/06.png"
                                             alt=""></div>
    </section>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif
<div class="row">
    <form class="form-inline" style="margin: auto;" action="{{url('/modifyConfirm')}}" method="post">
        @method('post')
        @csrf

        <div class="form-group mx-sm-3 mb-2">
            <label for="n" class="sr-only">N</label>
            <input type="number" class="form-control" name="n" id="n" placeholder="">
        </div>
        <div class="form-group mx-sm-3 mb-2">
            <label for="d" class="sr-only">d</label>
            <select class="form-control" name="d">
                <option value="day">Day</option>
                <option value="week">Week</option>
                <option value="month">Month</option>
            </select>
        </div>
        <div class="form-group mx-sm-3 mb-2">
            <label for="g" class="sr-only">g</label>
            <select class="form-control" name="g">
                <option value="individual">Individual</option>
                <option value="group">Group</option>
            </select>
        </div>
        <div class="form-group mx-sm-3 mb-2">
            <label for="tz" class="sr-only">TZ</label>
            <input type="text" class="form-control" name="tz" id="tz" placeholder="Time Zone">
        </div>
        <button type="submit" class="btn btn-theme">Confirm Setting</button>
    </form>
</div>

@endsection
