@extends('layouts.layout')
@section('script')

    <script type="text/javascript">


        function validation() {
            var ids = $('#input-tags').val();
            var user_ids = ids.split(",");
            for(var i=0; i<user_ids.length;i++) user_ids[i] = parseInt(user_ids[i], 10);
            var datetime = $('#date').val();
            var reservation_datetime = datetime.replace("T", " ");
            $.ajax({
                url: '/ReservationConfirm',
                data: {'user_ids': user_ids,'reservation_datetime':reservation_datetime},
                type: 'GET',

                success: function (data) {
                    console.log(data);

                }
            });
        }
    </script>
@endsection
@section('content')
    <section class="page-title o-hidden text-center grey-bg bg-contain animatedBackground"
             data-bg-img="/themes/base/frontassets/images/pattern/05.png">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-md-12">
                    <h1 class="title">Validate Reservation</h1>
                </div>
            </div>
        </div>
        <div class="page-title-pattern"><img class="img-fluid" src="themes/base/frontassets/images/bg/06.png"
                                             alt=""></div>
    </section>

    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif

    <div class="row" style="margin-top: 200px;">

        <form class="form-inline" style="margin: auto;" role="form" enctype="multipart/form-data">
            {{ csrf_field() }}

            <div class="form-group mx-sm-3 mb-2">
                <label for="users" class="sr-only">Users</label>
                    <input type="text" id="input-tags" class="form-control" style="width: 300px !important;" name="users[]" placeholder="Uesrs id" value="" required>
                <script>
                    $('#input-tags').selectize({
                        delimiter: ',',
                        persist: false,
                        create: function(input) {
                            return {
                                value: input,
                                text: input
                            }
                        }
                    });
                </script>

            </div>
            <div class="form-group mx-sm-3 mb-2">
                <input type="datetime-local" step="2" class="form-control" name="date" id="date" required>
            </div>

            <a onclick="validation()" class="btn btn-theme" style="color: #fff;">Confirm Reservation</a>
        </form>

    </div>

@endsection
