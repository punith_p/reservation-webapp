<!DOCTYPE html>
<html lang="en-US">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Validator </title>
    <link rel="icon" type="image/png" sizes="16x16" href="themes/base/frontassets/images/brand/favicon.png">
    <meta name="csrf-param" content="_csrf">
    <meta name="csrf-token"
          content="Xoww9tnDsD8bVB3Cz0dStq4Bk5bVpRQBh7PGZS5_-6US-gKbuPDxS30lRbKocjDR5FfW0pvWLHi3xq8DaSjD8g==">



    <link href="themes/base/frontassets/css/bootstrap.min.css" rel="stylesheet"/>
    <link href="themes/base/frontassets/css/animate.css" rel="stylesheet"/>
    <link href="themes/base/frontassets/css/fontawesome-all.css" rel="stylesheet"/>
    <link href="themes/base/frontassets/css/themify-icons.css" rel="stylesheet"/>

    <link href="themes/base/frontassets/css/magnific-popup/magnific-popup.css" rel="stylesheet"/>
    <link href="themes/base/frontassets/css/owl-carousel/owl.carousel.css" rel="stylesheet"/>
    <link href="themes/base/frontassets/css/base.css" rel="stylesheet"/>

    <link href="themes/base/frontassets/css/shortcodes.css" rel="stylesheet"/>
    <link href="themes/base/frontassets/css/style.css" rel="stylesheet"/>
    <link href="themes/base/frontassets/css/responsive.css" rel="stylesheet"/>

    <script src="themes/base/frontassets/js/jquery.min.js"></script>

    <script src="assets/a628c5a8/yii.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.form/4.2.2/jquery.form.min.js"></script>

    <script src="assets/ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>
    <script src="assets/ajax.googleapis.com/ajax/libs/jqueryui/1.10.3/jquery-ui.min.js"></script>
    <script src="assets/js/highlight.min.js"></script>

    <link rel="stylesheet" href="assets/css/selectize.default.css" data-theme="default">
    <script src="assets/js/selectize.js"></script>
</head>
<body>


<header id="site-header" class="header">
    <div id="header-wrap">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg">
                        <h4><a class="navbar-brand logo" href="#">
                                validation
                            </a></h4>
                        <button class="navbar-toggler" type="button" data-toggle="collapse"
                                data-target="#navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span></span>
                            <span></span>
                            <span></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto mr-auto">
                                <li class="nav-item" data-toggle="hover"><a class="nav-link" href="#">Home</a></li>
                                <li class="nav-item" data-toggle="hover"><a class="nav-link" href="#">About Us</a></li>
                                <li class="nav-item" data-toggle="hover"><a class="nav-link" href="{{url('modify')}}">Reservation</a>
                                </li>
                            </ul>
                        </div>
                        @guest

                                <a class="btn btn-white btn-sm" href="{{ route('login') }}">Login</a>

                            @if (Route::has('register'))

                                    <a class="btn btn-white btn-sm" href="{{ route('register') }}">Sign Up</a>

                            @endif
                        @else

                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item btn btn-theme" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                        @endguest
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>
