@extends('layouts.layout')

@section('content')

<div class="page-wrapper">


    <div class="page-wrapper">

        <section class="page-title o-hidden text-center grey-bg bg-contain animatedBackground"
                 data-bg-img="/themes/base/frontassets/images/pattern/05.png">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <h1 class="title">Sign Up</h1>
                        <nav aria-label="breadcrumb" class="page-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item">Sign Up</li>
                                <li class="breadcrumb-item active" aria-current="page"><a href="{{ route('login') }}">Login</a></li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="page-title-pattern"><img class="img-fluid" src="themes/base/frontassets/images/bg/06.png"
                                                 alt=""></div>
        </section>

        <div class="page-content">

            <section class="login">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-10 ml-auto mr-auto">
                            <div class="login-form box-shadow white-bg px-5 py-5 xs-px-2 xs-py-2">
                                <div class="text-center">
                                    <h2 class="title mb-5">User Register</h2>
                                </div>
                                <form id="w0" action="{{ route('register') }}" method="post">
                                    @csrf
                                    <div class="form-group field-user-full_name required">
                                        <label class="control-label" for="user-full_name">Full Name</label>
                                        <input id="user-full_name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                        @error('name')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group field-user-email required">
                                        <label class="control-label" for="user-email">Email</label>
                                        <input id="user-email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group field-user-password required">
                                        <label class="control-label" for="user-password">Password</label>
                                        <input id="user-password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group field-profile-hotel_name required">
                                        <label class="control-label" for="profile-hotel_name">Confirm Password</label>
                                        <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                        <div class="invalid-feedback"></div>
                                    </div>

                                    <div class="text-center">
                                        <button type="submit" class="btn btn-theme btn-lg" name="login-button">Signup
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>

    </div>
</div>

@endsection
