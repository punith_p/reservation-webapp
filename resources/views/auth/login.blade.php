@extends('layouts.layout')
@section('content')

        <section class="page-title o-hidden text-center grey-bg bg-contain animatedBackground"
                 data-bg-img="/themes/base/frontassets/images/pattern/05.png">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-12">
                        <h1 class="title">Login</h1>
                        <nav aria-label="breadcrumb" class="page-breadcrumb">
                            <ol class="breadcrumb">
                                <li class="breadcrumb-item"><a href="{{ route('register') }}">Sign Up</a>
                                </li>
                                <li class="breadcrumb-item active" aria-current="page">Login</li>
                            </ol>
                        </nav>
                    </div>
                </div>
            </div>
            <div class="page-title-pattern"><img class="img-fluid" src="themes/base/frontassets/images/bg/06.png"
                                                 alt=""></div>
        </section>

        <div class="page-content">

            <section class="login">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6 col-md-10 ml-auto mr-auto">
                            <div class="login-form box-shadow white-bg px-5 py-5 xs-px-2 xs-py-2">
                                <div class="text-center">
                                    <h2 class="title mb-5">Login</h2>
                                </div>
                                <form id="login-form" action="{{ route('login') }}" method="post">
                                    @csrf
                                    <div class="form-group field-loginform-username required">
                                        <label class="control-label" for="loginform-username">Email</label>
                                        <input type="email" id="loginform-username" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                        @error('email')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="form-group field-loginform-password required">
                                        <label class="control-label" for="loginform-password">Password</label>
                                        <input id="loginform-password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                                        @error('password')
                                        <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                        </span>
                                        @enderror

                                        <div class="invalid-feedback"></div>
                                    </div>
                                    <div class="custom-control custom-control-alternative custom-checkbox">
                                        <div class="form-group field-loginform-rememberme">

                                            <div class="invalid-feedback"></div>
                                        </div>
                                    </div>
                                    <div class="text-center">
                                        <button type="submit" class="btn btn-theme btn-lg" name="login-button">Login
                                        </button>
                                    </div>
                                </form>

                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>

@endsection
