<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestrictionSettingMigration extends Migration
{
    public function up()
    {
        Schema::create('restriction_setting', function (Blueprint $table) {
            $table->id();
            $table->integer('n')->default(NULL);
            $table->enum('d',['day','week','month'])->default('day');
            $table->enum('g',['individual','group'])->default('group');
            $table->char('tz',255)->default('UTC');
            $table->timestamps();
        });

        Schema::table('reservation_setting', function (Blueprint $table){
            DB::statement('
INSERT INTO restriction_setting (id,n,d, g, tz) VALUES (1,2,\'day\',\'individual\',\'Asia/Kolkata\');
');
        });
    }

    public function down()
    {
        Schema::dropIfExists('restriction_setting');
    }
}
